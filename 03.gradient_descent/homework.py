import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.sparse.construct import random
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import roc_curve
from scipy.special import softmax

def sigmoid(x):
    if x > 10:
        x = 10
    if x < -10:
        x = -10
    return 1/(1+math.exp(-x))


def predict(W, x):
    Prediction = []
    for row in range(0, len(x)):
        Pred = W[0]
        for i in range(1, len(W)):
            Pred += x[row][i-1]*W[i]
             
        Pred_class = 0
        Pred_sig = sigmoid(Pred)
        if Pred_sig > 0.5:
            Pred_class = 1
        Prediction.append(sigmoid(Pred_class))
    return softmax(Prediction)


def checkModel(W, x, y):
    # W0 is bias
    error_count = 0
    for row in range(0, len(x)):
        Pred = W[0]
        for i in range(1, len(W)):
            Pred += x[row][i-1]*W[i]
        Pred_class = 0
        Pred_sig = sigmoid(Pred)
        if Pred_sig > 0.5:
            Pred_class = 1
        Expect = y.iloc[row]
        if Pred_class != Expect:
            error_count += 1
    return 1.00 - error_count/(len(y) * 1.00)


def learnGDMomentum(x, y, step, th,  iterations=100, B=0.1):
    # 0 is bias
    W = np.zeros(len(x[0]) + 1)
    VelPrev = np.zeros(len(x[0]) + 1)
    for iteration in range(iterations):
        for row in range(0, len(x)):
            Pred = W[0]
            for i in range(1, len(W)):
                Pred += x[row][i-1]*W[i]
            Pred_sig = sigmoid(Pred)
            Expect = y.iloc[row]
            error = Pred_sig - Expect
            Vel = np.zeros(len(x[0]) + 1)
            Vel[0] -= step * error + VelPrev[0] * B
            W[0] += Vel[0]
            for i in range(1, len(W)):
                Vel[i] = -step * error * x[row][i-1] + VelPrev[i] * B
                W[i] += Vel[i]
            if np.all(np.abs(Vel) < th):
                print("Quick return iteration %d row %d" % (iteration, row))
                return W
            VelPrev = Vel
    return W


def learnGD(x, y, step, th, iterations=100):
    # W0 is bias
    W = np.zeros(len(x[0]) + 1)
    for iteration in range(iterations):
        for row in range(0, len(x)):
            Pred = W[0]
            for i in range(1, len(W)):
                Pred += x[row][i-1]*W[i]
            Pred_sig = sigmoid(Pred)
            Expect = y.iloc[row]
            error = Pred_sig - Expect
            Delta = np.zeros(len(x[0]) + 1)
            Delta[0] = -step * error
            W[0] += Delta[0]
            for i in range(1, len(W)):
                Delta[i] = -step * error * x[row][i-1]
                W[i] += Delta[i]
            if np.all(np.abs(Delta) < th):
                print("Quick return iteration %d row %d" % (iteration, row))
                return W
    return W


def learnRprop(x, y, step, th,  iterations=100, B=0.1,  incfactor=1.2, decfactor=0.5):
    def findNewGrad(Prevgrad, Grad, incFactor, decfactor, step):
        minstep = step * 0.1
        maxstep = step * 10
        n = len(Prevgrad)
        NewGrad = np.zeros(n)
        for i in range(n):
            cur = Grad[i]
            prev = Prevgrad[i]

            if prev * cur > 0:
                if cur > 0:
                    NewGrad[i] = min(incFactor * cur, maxstep)
                if cur < 0:
                    NewGrad[i] = max(incFactor * cur, maxstep * -1)
            if prev * cur < 0:
                if cur > 0:
                    NewGrad[i] = max(decfactor * cur, minstep)
                if cur < 0:
                    NewGrad[i] = min(decfactor * cur, minstep * -1)
            if prev*cur == 0:
                NewGrad[i] = cur
        return NewGrad
  # W0 is bias
    Prevgrad = np.zeros(len(x[0]) + 1)
    W = np.zeros(len(x[0]) + 1)
    for iteration in range(iterations):
        for row in range(0, len(x)):
            Pred = W[0]
            for i in range(1, len(W)):
                Pred += x[row][i-1]*W[i]
            Pred_sig = sigmoid(Pred)
            Expect = y.iloc[row]
            error = Pred_sig - Expect
            Grad = np.zeros(len(x[0]) + 1)
            Grad[0] = -step * error
            for i in range(1, len(W)):
                Grad[i] = -step * error * x[row][i-1]
            Newgrad = findNewGrad(Prevgrad, Grad, incfactor, decfactor, step)
            for k in range(len(W)):
                W[k] = Newgrad[k]
            if np.all(np.abs(Newgrad) < th):
                print("Quick return iteration %d row %d" % (iteration, row))
                return W
            Prevgrad = Grad
    return W


data = pd.read_csv('./03.gradient_descent/iris.csv')
data = data[(data['variety'] == 'Versicolor') |
            (data['variety'] == 'Virginica')]

le = LabelEncoder()
data['variety'] = le.fit_transform(data['variety'])
y = data['variety']
del data['variety']

data = StandardScaler().fit_transform(data)
x_train, x_test, y_train, y_test = train_test_split(
    data, y, test_size=0.2, random_state=6)

print("GDM:")
resultGDM = learnGDMomentum(x_train, y_train, 1, 0.0001)
train_accGDM = checkModel(resultGDM, x_train, y_train)
test_accGDM = checkModel(resultGDM, x_test, y_test)
print("weights %s" % resultGDM)
print("train accuracy: %f" % train_accGDM)
print("test accuracy: %f" % test_accGDM)

print("\nGD:")
resultGD = learnGD(x_train, y_train, 1, 0.0001)
train_accGD = checkModel(resultGD, x_train, y_train)
test_accGD = checkModel(resultGD, x_test, y_test)
print("weights %s" % resultGD)
print("train accuracy: %f" % train_accGD)
print("test accuracy: %f" % test_accGD)

print("\nRprop:")
resultRprop = learnRprop(x_train, y_train, 1, 0.0001)
train_accRprop = checkModel(resultRprop, x_train, y_train)
test_accRprop = checkModel(resultRprop, x_test, y_test)
print("weights %s" % resultRprop)
print("train accuracy: %f" % train_accRprop)
print("test accuracy: %f" % test_accRprop)


def roc_curve1(y_true, y_prob, thresholds):

    fpr = []
    tpr = []
    p = np.sum(y_true == 1)
    n = np.sum(y_true == 0)
    thresholds[::-1].sort()
    # for i in range(len(y_true)):
    #     print("t %d p %f" % (y_true.iloc[i], y_prob[i]))
    thresholds = np.insert(thresholds, 0, thresholds[0]+1)
    for threshold in thresholds:

        tp = np.sum((y_prob >= threshold) & (y_true == 1))

        tn = np.sum((y_prob <= threshold) & (y_true == 0))

        fpr_item = 1-(tn*1.0)/n
        tpr_item = (tp * 1.0) / (p)
        # print("item_ %d %d %d %d" % (tp, fp, tn, fn))
        # print("item %f %f %f" % (fpr_item, tpr_item, threshold))

        fpr.append(fpr_item)
        tpr.append(tpr_item)

    return (fpr, tpr)

predictions = predict(resultGDM, x_test)

fpr1, tpr1 = roc_curve1(y_test[:], predictions[:], predictions[:])
fpr, tpr, th = roc_curve(y_test[:], predictions[:])

fpr1.sort()
tpr1.sort()
plt.plot(fpr1, tpr1)
plt.plot(fpr, tpr)

plt.grid()
plt.show()

# GDM:
# Quick return iteration 3 row 41
# weights [-0.38230907 -1.61171728 -0.87792517  4.5218069   5.31412317]
# train accuracy: 0.987500
# test accuracy: 0.900000

# GD:
# Quick return iteration 1 row 43
# weights [-0.42945904 -1.22637164 -0.59224174  3.33216902  4.18940958]
# train accuracy: 0.975000
# test accuracy: 0.900000

# Rprop:
# weights [-0.35935646  1.65033985  0.94646588  0.61508456  1.37947389]
# train accuracy: 0.775000
# test accuracy: 0.900000
