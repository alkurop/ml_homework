import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt


def f(x): return x*x*0.5 + x
def dx(x): return x + 1


x = np.linspace(-50, 50, 1000)


def getMin(x0, step, threshhold, f, f_def):
    l = [[x0, f(x0)]]
    while True:
        x1 = l[len(l) - 1][0]
        delta = f_def(x1)
        x2 = x1-delta * step

        if np.abs(delta) < threshhold or len(l) > 100:
            break
        l.append([x2, f(x2)])
    return l[len(l) - 1][0], np.array(l)

x0 = 2
threshhold = 0.01
step = 0.5
min, res = getMin(x0, step, threshhold, f, dx)


print(res[:, 1])

plt.plot(x, f(x))
plt.plot(x, dx(x))
plt.plot(res[:, 0], res[:, 1])

plt.xlim([-2, 8])
plt.ylim([-2, 8])
# plt.show()
