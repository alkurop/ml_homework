import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
import pandas as pd

data = pd.read_csv('./sat.txt',sep=' ')

x = data[['high_GPA']]
y = data['univ_GPA']
regr = linear_model.LinearRegression()
regr.fit(x,y)

print(type(x))
print(type(y))

plt.scatter(x,y)
plt.plot(x, regr.predict(x), color = 'red')
# plt.show()