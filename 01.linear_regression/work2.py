import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt

from datetime import datetime
import calendar


def stringToTimestamp(input):
    date_obj = datetime.strptime(input, '%m/%d/%y')
    return calendar.timegm(date_obj.timetuple())


data = pd.read_csv('./athletes.csv')

data = data[(pd.isnull(data['height']) == False) &
            (pd.isnull(data['weight']) == False)]


x = data[['nationality', 'sex', 'dob', 'height', 'weight']]
x = pd.get_dummies(x, columns=['nationality'])
x = pd.get_dummies(x, columns=['sex'])
x['dob'] = x['dob'].apply(lambda x: stringToTimestamp(x))

le = LabelEncoder()
le.fit(data['sport'])
y = pd.Series(data=le.transform(data['sport']))


x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.33)

model = LogisticRegression()
model.fit(x_train, y_train)
pred = pd.Series(data=model.predict(x_test))

# Does not work - x has more then 1 dimentions
# plt.scatter(x_test, pred)
# plt.plot(x_test, model.predict(x_test), color='red')
# plt.show()

print(model.score(x_test, y_test))
# 0.20172991071428573