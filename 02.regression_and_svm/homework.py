import pandas
from sklearn.linear_model.base import LinearRegression
from sklearn.svm import SVC
from sklearn.pipeline import make_pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

data = pandas.read_csv("adult.csv")

le = LabelEncoder()
le.fit(data['income'])
y = pandas.Series(data=le.transform(data['income']))


def predictByEducation():
    x = data[['educational-num']]
    model = make_pipeline(LogisticRegression())
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=69)

    model.fit(x_train, y_train)

    trainScore = model.score(x_train, y_train)
    testScore = model.score(x_test, y_test
                            )
    print("train_score = %f" % trainScore)
    print("test_score = %f" % testScore)
    # train_score = 0.780046
    # test_score = 0.779021


def predictByRace():
    x = data[['race']]
    x = pandas.get_dummies(x, columns=['race'])

    model = make_pipeline(LogisticRegression())
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=69)

    model.fit(x_train, y_train)

    trainScore = model.score(x_train, y_train)
    testScore = model.score(x_test, y_test)

    print("train_score = %f" % trainScore)
    print("test_score = %f" % testScore)
    # train_score = 0.761297
    # test_score = 0.759367


def predictByOccupation():
    x = data[['occupation']]
    x = pandas.get_dummies(x, columns=['occupation'])

    model = make_pipeline(LogisticRegression())
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=69)

    model.fit(x_train, y_train)

    trainScore = model.score(x_train, y_train)
    testScore = model.score(x_test, y_test
                            )
    print("train_score = %f" % trainScore)
    print("test_score = %f" % testScore)
    # train_score = 0.761297
    # test_score = 0.759367

def getXWithMultipleParams():
    x = data
    # this is our target
    del x['income']

    # wtf is fnlwgt?
    del x['fnlwgt']

    # we are using educational-num
    del x['education']
    return pandas.get_dummies(x, columns=['marital-status', 'occupation',
                                       'relationship', 'race', 'gender', 'native-country', 'workclass'])


def predictByMultipleParamsLinearRegression():
    x = getXWithMultipleParams()

    model = make_pipeline(LinearRegression())
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=69)

    model.fit(x_train, y_train)

    trainScore = model.score(x_train, y_train)
    testScore = model.score(x_test, y_test
                            )
    print("train_score = %f" % trainScore)
    print("test_score = %f" % testScore)

    # train_score = 0.852028
    # test_score = 0.850474


def predictByMultipleParamsSVMwithStandartScaler():
    x = getXWithMultipleParams()

    model = make_pipeline(StandardScaler(), SVC())
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=69)

    model.fit(x_train, y_train)

    trainScore = model.score(x_train, y_train)
    testScore = model.score(x_test, y_test)
    print("train_score = %f" % trainScore)
    print("test_score = %f" % testScore)

    # train_score = 0.858609
    # test_score = 0.849724

def predictByMultipleParamsSVMwithoutStandartScaler():
    x = getXWithMultipleParams()

    model = make_pipeline(SVC())
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=69)

    model.fit(x_train, y_train)

    trainScore = model.score(x_train, y_train)
    testScore = model.score(x_test, y_test)
    print("train_score = %f" % trainScore)
    print("test_score = %f" % testScore)

    # train_score = 0.872181
    # test_score = 0.864533

# predictByEducation()
# predictByRace()
# predictByOccupation()
# predictByMultipleParamsLinearRegression()
# predictByMultipleParamsSVMwithStandartScaler()
predictByMultipleParamsSVMwithoutStandartScaler()
