import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.sparse.construct import random
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve
from scipy.special import softmax

data = pd.read_csv('./01.linear_regression/athletes.csv')

data = data[(pd.isnull(data['height']) == False) &
            (pd.isnull(data['weight']) == False)]

data['height'].unique()

features_columns = [c for c in data.columns if c != 'sex']

le = LabelEncoder()
ohe = OneHotEncoder()

ft = ohe.fit_transform(
    data[['nationality', 'height', 'weight', 'sport', 'gold', 'silver', 'bronze']])
X = data[['height', 'weight', 'gold', 'silver', 'bronze']]

le.fit(data['sex'])
y = pd.Series(data=le.transform(data['sex']))
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.3, random_state=1)
model = LogisticRegression()
model.fit(X_train, y_train)
predictions = model.predict_proba(X_test)

fpr, tpr, thres = roc_curve(
    y_test[:10], predictions[:10, 1], drop_intermediate=False)


def roc_curve1(y_true, y_prob):
    fpr = []
    tpr = []
    positive = np.sum(y_true == 1)
    negative = np.sum(y_true == 0)
    for threshold in y_prob:
        tp = np.count_nonzero((y_prob >= threshold) & (y_true == 1))
        tn = np.count_nonzero((y_prob <= threshold) & (y_true == 0))
        fpr_item = 1 - (tn * 1.0)/negative
        tpr_item = (tp * 1.0) / positive
        fpr.append(fpr_item)
        tpr.append(tpr_item)
    fpr.sort()
    tpr.sort()
    y_prob.sort()
    return (fpr, tpr)


fpr1, tpr1 = roc_curve1(y_test[:], predictions[:, 1])

# plt.plot(fpr, tpr)
# plt.show()
# contact Daniel for nullability on the backend

plt.plot(fpr1, tpr1)
plt.grid()
plt.show()
