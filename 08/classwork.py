import math
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.sparse.construct import random
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_curve
from scipy.special import softmax

data = pd.read_csv('./08/new_data2.csv')
print(data)
